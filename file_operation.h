#ifndef FILE_OPERATION_H
#define FILE_OPERATION_H

#include <fstream>
#include <iostream>
#include <regex>

class File_operation {
	static std::stringstream remove_comments(std::ifstream& file)
	{
		std::string line;
		std::stringstream ss;
		while (std::getline(file, line)) {
			std::size_t pos = line.find("--");
			if (pos != std::string::npos) {
				ss << line.substr(0, pos);
				if (pos != 0) {
					//comment is not for whole line, 
					ss << "\n";
				}
			} else {
				ss << line + "\n";
			}
		}
		return ss;
	}

public:
	static std::stringstream get_file(std::string path) {
		std::ifstream file_temp {path};
		if (file_temp.is_open()) {
			std::stringstream file = remove_comments(file_temp);
			file_temp.close();
			return file;
		} else {
			std::cerr << "Unable to open file: " << path; 
			throw; 
		}
	}
};

#endif