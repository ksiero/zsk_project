#ifndef IMPORTS_H
#define IMPORTS_H

#include <algorithm>
#include <cctype>
#include <fstream>
#include <iostream>
#include <list>
#include <regex>
#include <string>
#include <vector>

#include "file_operation.h"
#include "group_node.h"
#include "object_node.h"
#include "type.h"

struct Imports {
	static void load_import_types(std::stringstream& file, std::string imports_line, std::list<Type>& types, Group_node& root_node) {
		//imports_line - line with IMPORTS
		std::string imports_scope = imports_line;
		std::string line;
		bool in_import_scope = true;
		while (in_import_scope) {
			std::getline(file, line);
			imports_scope += line + "\n";  //std::getline removes end of line
			std::regex semicolon_regex{";"};
			if (std::regex_search(line, semicolon_regex)) {
				in_import_scope = false;
			}
		}

		std::string from;
		std::smatch match;
		std::regex from_regex{"FROM\\s+(.*)"};
		std::regex_search(imports_scope, match, from_regex);
		from = match[1];

		std::stringstream imported_file = File_operation::get_file(from + ".txt");
		// std::cout << "Parsing file types from: " << from << std::endl;
		parse_file_types(imported_file, types, root_node);
		// std::cout << "End of imported from: " << from << std::endl;
	}

	static void skip_import(std::stringstream& file, std::string imports_line) {
		//imports_line - line with IMPORTS
		std::string imports_scope = imports_line;
		std::string line;
		bool in_import_scope = true;
		while (in_import_scope) {
			std::getline(file, line);
			imports_scope += line + "\n";  //std::getline removes end of line
			std::regex semicolon_regex{";"};
			if (std::regex_search(line, semicolon_regex)) {
				in_import_scope = false;
			}
		}
	}

	static void parse_file_types(std::stringstream& file, std::list<Type>& types, Group_node& root_node) {
		std::string line;
		while (std::getline(file, line)) {
			line += "\n"; //std::getline removes end of line
			//std::cout << "Line: " << line;
			std::regex import_keyword {"IMPORTS"};	
			if (std::regex_search(line, import_keyword)) {
				load_import_types(file, line, types, root_node);
				continue;
			}

			std::regex macro_keyword {"MACRO"};	
			if (std::regex_search(line, macro_keyword)) {
				skip_macro(file, line);
				continue;
			}

			std::regex group_node_keyword {"OBJECT IDENTIFIER"};
			if (std::regex_search(line, group_node_keyword)) {
				Group_node::create_group_node(file, line, root_node);
				continue;
			}

			std::regex object_node_keyword {"\\w\\s+OBJECT-TYPE"}; // can erase \\w if not loading EXPORTS
			if (std::regex_search(line, object_node_keyword)) {
				Object_node::skip_object_node(file, line);
				continue;
			}

			//must be the last one, if found ::= means type asign
			std::regex type_node_keyword {"::="};
			if (std::regex_search(line, type_node_keyword)) {
				std::regex definition_regex {"DEFINITIONS"};
				if (!std::regex_search(line, definition_regex)) { 
					// not a definition => type
					types.emplace_back(file, line, types);
				}
			}
		}

	}

	static void parse_file_nodes(std::stringstream& file, Group_node& root_node, std::list<Type>& types) {
		std::string line;
		while (std::getline(file, line)) {
			line += "\n"; //std::getline removes end of line
			std::regex import_keyword {"IMPORTS"};	
			if (std::regex_search(line, import_keyword)) {
				skip_import(file, line);
				continue;
			}

			std::regex macro_keyword {"MACRO"};	
			if (std::regex_search(line, macro_keyword)) {
				skip_macro(file, line);
				continue;
			}

			std::regex group_node_keyword {"OBJECT IDENTIFIER"};
			if (std::regex_search(line, group_node_keyword)) {
				Group_node::skip_group(file, line);
				continue;
			}

			std::regex object_node_keyword {"OBJECT-TYPE"};
			if (std::regex_search(line, object_node_keyword)) {
				Object_node o_node {file, line, types};
				///o_node.debug_print();
				root_node.put_node(std::move(o_node));
				continue;
			}

			//must be the last one, if found ::= means type asign
			std::regex type_node_keyword {"::="};
			if (std::regex_search(line, type_node_keyword)) {
				//create only to skip these lines (I know, not optimal :( )
				Type new_type {file, line, types};
			}
		}

	}

private:
	static void skip_macro(std::stringstream& file, std::string line) {
		bool in_macro = true;
		while (in_macro) {
			getline(file, line);
			std::regex break_regex {"END"};
			if (std::regex_search(line, break_regex)) {
				in_macro = false;
			}
		}
	}

	// void load_imports(std::stringstream& file, std::string imports_line, std::vector<Node>& root_node, std::vector<Type>& types) {
	// 	//imports_line - line with IMPORTS
	// 	std::string imports_scope = imports_line;
	// 	std::string line;
	// 	bool in_import_scope = true;
	// 	while (in_import_scope) {
	// 		std::getline(file, line);
	// 		imports_scope += line + "\n";  //std::getline removes end of line
	// 		std::regex semicolon_regex{";"};
	// 		if (std::regex_search(line, semicolon_regex)) {
	// 			in_import_scope = false;
	// 		}
	// 	}

	// 	// //take everything between IMPORTS and first FROM
	// 	// std::regex imports_names_regex{"IMPORTS((.|\n)*?)FROM"};
	// 	// std::smatch match;
	// 	// std::regex_search(imports_scope, match, imports_names_regex);
	// 	// std::string imports_names = match[1];

	// 	// //delete whitespace
	// 	// imports_names.erase(std::remove_if(imports_names.begin(), imports_names.end(), ::isspace), imports_names.end());

	// 	// //split string with ','
	// 	// std::stringstream ss {imports_names};
	// 	// while (getline(ss, line, ',')) {
	// 	// 	if (line[0] > 'a' && line[0] < 'z') {
	// 	// 		root_node.emplace_back(line);
	// 	// 	} else if  (line[0] > 'A' && line[0] < 'Z') {
	// 	// 		types.emplace_back(line);
	// 	// 	} else {
	// 	// 		throw std::string{"Cannot import "} + line + std::string{", didn't expect that :()"};
	// 	// 	}
	// 	// }

	// 	//take first after FROM
	// 	std::string from;
	// 	std::smatch match;
	// 	std::regex from_regex{"FROM\\s+(.*)"};
	// 	std::regex_search(imports_scope, match, from_regex);
	// 	from = match[1];

	// 	std::cout << "Importing: " << from << std::endl;
	// 	std::stringstream imported_file = get_file(from + ".txt");
	// 	std::cout << "Parsing imported file: " << from << std::endl;
	// 	Context::parse_file(imported_file, root_node, types);
	// 	std::cout << "Imported file parsed: " << from << std::endl;
	// }

};

#endif