#include <fstream>
#include <iostream>
#include <regex>
#include <memory>

#include "context.h"
#include "snmp_msg.h"

uint8_t hex2bin(std::string hex) {
	int x;
	std::stringstream ss {hex};
	ss >> std::hex >> x;
	return static_cast<uint8_t>(x);
}

int main(int argc, char* argv[])
{
	//Context global_context {"RFC1213-MIB.txt"};

	// std::string oid;
	// std::string ber;
	// //std::cout << "Give OID: ";
	// oid = "1.3.6.1.2.1.1.1"; //"1.3.6.1.2.1.2.3";
	// //std::cin >> oid
	// std::cout << "Give: ";
	// std::cin >> ber;

	// std::unique_ptr<uint8_t[]> bytes { new uint8_t[ber.size()/2] };

	// for (int i = 0; i < ber.size()/2; i++) {
	// 	bytes[i] = hex2bin(ber.substr(i*2, 2));
	// }
	
	//global_context.decode(oid, bytes.get());
	//global_context.encode(oid, bytes.get(), ber.size()/2);

	std::cout << "Give pdu type (0,2,3): ";
	std::string pdu_type = "3";
	//std::cin >> pdu_type;

	std::cout << "Give OID: ";
	std::string oid = "1.3.6.1.2.1.1.1";
	//std::cin >> oid;

	std::cout << "Give Value: ";
	std::string value = "A1B2";
	//std::cin >> value;

	int req_id = 1044;

	std::unique_ptr<uint8_t[]> bytes { new uint8_t[value.size()/2] };

	for (int i = 0; i < value.size()/2; i++) {
		bytes[i] = hex2bin(value.substr(i*2, 2));
	}

	Snmp_msg snmp;
	//snmp.decode(bytes.get());
	snmp.encode(std::stoi(pdu_type), req_id, oid, bytes.get(), value.size()/2);

	return 0;
}
