#include <string>
#include <iostream>
#include "snmp_msg.h"

#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 

uint8_t hex2bin(std::string hex) {
	int x;
	std::stringstream ss {hex};
	ss >> std::hex >> x;
	return static_cast<uint8_t>(x);
}

int main()
{
    struct sockaddr_in server_addr;
    memset(&server_addr, 0, sizeof(server_addr)); 
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(161);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

    int socket_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (socket_fd < 0){
        perror("Error while openning a socket");
        return 1;
    }
   
	//std::string operation = "1";
	std::string oid_str = "1.3.6.1.2.1.1.3";
	//std::string value = "A1B2";
    // std::string oid_str;
    // std::cout << "OID: "; std::cin >> oid_str;
    std::string operation;
    std::cout << "Operation (0 - Get, 1 - GetNext, 2 - Set): "; std::cin >> operation;
    std::string value;
    if (operation == "2") {
        std::cout << "Value: "; std::cin >> value;
    }

    int pdu_type;
    if (std::stoi(operation) == 1) {
        pdu_type = 1;
    } else if (std::stoi(operation) == 2) {
        pdu_type = 3;
    } else {
        pdu_type = 0;
    }

	Snmp_msg snmp;
    std::vector<uint8_t> msg;
    if (pdu_type == 3) {
        //set
        std::unique_ptr<uint8_t[]> bytes { new uint8_t[value.size()/2] };

        for (int i = 0; i < value.size()/2; i++) {
            bytes[i] = hex2bin(value.substr(i*2, 2));
        }
	    msg = snmp.encode(pdu_type, 1, oid_str, bytes.get(), value.size()/2);
    } else {
        //get
        msg = snmp.encode_get(pdu_type, 1, oid_str);
    }

    printf("Sending: ");
    for (auto x : msg) {
		printf("%02x", x);
	}
	std::cout << std::endl;

    if (sendto(socket_fd, msg.data(), msg.size(), 0, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0 ) {
        perror("Cannot send msg");
        close(socket_fd);
        return 1;
    }

    socklen_t size = sizeof(server_addr);
    uint8_t buffer[4096];
    auto received_bytes = recvfrom(socket_fd, buffer, 4096, 0, (struct sockaddr*) &server_addr, &size);
    if (received_bytes < 0) {
        perror("Cannot receive msg");
        close(socket_fd);
        return 1;
    }

    printf("Received: ");
    for (int i = 0; i < received_bytes; i++) {
		printf("%02x", buffer[i]);
	}
	std::cout << std::endl;
    snmp.decode(buffer);

    close(socket_fd);
    return 0;
}