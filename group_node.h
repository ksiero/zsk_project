#ifndef GROUP_NODE_H
#define GROUP_NODE_H

#include <algorithm>
#include <cctype>
#include <fstream>
#include <iostream>
#include <regex>
#include <string>
#include <vector>

#include "object_node.h"

class Group_node {
	long m_id {-1}; // id in parent (last digit in OID)
	std::string m_name;
	std::string m_parent_name;  // is this necessary?

	std::vector<Group_node> m_group_children;
	std::vector<Object_node> m_object_children;

public:
	Group_node()
	{}

	Group_node(std::string name) :
		m_name{name}
	{}

	Group_node(long id, std::string name) :
		m_id{id},
		m_name{name}
	{}

	Group_node(long id, std::string name, std::string parent_name) :
		m_id{id},
		m_name{name},
		m_parent_name{parent_name}
	{}

	bool put_node(Group_node node_to_put) {
		if (node_to_put.m_parent_name == m_name) {
			if (m_object_children.size()) {
				std::cerr << m_name << ": cannot put " << node_to_put.m_name << "node, I am object holder!\n";
				throw;
			}
			// std::cout << m_name << " puts child " << node_to_put.m_name << std::endl;
			for (const auto& child : m_group_children) {
				if (child.m_name == node_to_put.m_name && (node_to_put.m_id == -1 || child.m_id == node_to_put.m_id)) {
					// I already have this child
					return true;
				}
			}
			node_to_put.m_parent_name = m_name;
			//node_to_put.debug_print();
			m_group_children.push_back(node_to_put);
			return true;
		} else {
			//std::cout << m_name << " need to ask my " << m_group_children.size() << " children about " << node_to_put.m_name << std::endl;
			for (auto& child : m_group_children) {
				///std::cout << m_name << " asking " << child.m_name << std::endl; 
				if (child.put_node(node_to_put)) {
					//std::cout << m_name << ": My child " << child.m_name << " found " << node_to_put.m_name << std::endl;
					return true;
				}
			}
		}
		return false;
	}

	bool put_node(Object_node node_to_put) {
		if (node_to_put.get_parent_name() == m_name) {
			if (m_group_children.size()) {
				std::cerr << m_name <<": cannot put " << node_to_put.get_name() << "node, I am group holder!\n";
				throw;
			}
			// std::cout << m_name << " puts child " << node_to_put.m_name << std::endl;
			for (const auto& child : m_object_children) {
				if (child.get_name() == node_to_put.get_name() && (node_to_put.get_id() == -1 || child.get_id() == node_to_put.get_id())) {
					// I already have this child
					return true;
				}
			}
			node_to_put.get_parent_name() = m_name;
			//node_to_put.debug_print();
			m_object_children.push_back(node_to_put);
			return true;
		} else {
			//std::cout << m_name << " need to ask my " << m_group_children.size() << " children about " << node_to_put.m_name << std::endl;
			for (auto& child : m_group_children) {
				///std::cout << m_name << " asking " << child.m_name << std::endl; 
				if (child.put_node(node_to_put)) {
					//std::cout << m_name << ": My child " << child.m_name << " found " << node_to_put.m_name << std::endl;
					return true;
				}
			}
		}
		return false;
	}

	void debug_print() const {
		std::cout << m_name <<":\n\tID: " << m_id << std::endl;
		if (m_group_children.size()) {
			std::cout << "\tType: Group node" << std::endl;
		} else if (m_object_children.size()) {
			std::cout << "\tType: Object node" << std::endl;
		} else {
			std::cout << "\tType: UNDEFINED" << std::endl;
		}
		std::cout << "\tParent name: " << m_parent_name << std::endl;

		if (m_group_children.size()) {
			std::cout << "\tGroup children:" << std::endl;
			for (const auto& child : m_group_children) {
				std::cout << "\t\t" << child.m_name << std::endl;
			}
			for (const auto& child : m_group_children) {
				child.debug_print();
				std::cout << std::endl;
			}
		}

		if (m_object_children.size()) {
			std::cout << "\tObject children:" << std::endl;
			for (const auto& child : m_object_children) {
				std::cout << "\t\t" << child.get_name() << std::endl;
			}
			for (const auto& child : m_object_children) {
				child.debug_print();
				std::cout << std::endl;
			}
		}
	}

	static void create_group_node(std::stringstream& file, std::string id_line, Group_node& root_node)
	{
		//id_line - line with OBJECT IDENTIFIER
		
		std::regex name_regex{"\\s*(.*)\\s+OBJECT IDENTIFIER"};
		std::smatch match;
		std::regex_search(id_line, match, name_regex);
		std::regex space_regex{"\\s+"};
		std::string defined_name = std::regex_replace(match[1].str(), space_regex, " ");
		
		std::regex tree_regex{"OBJECT IDENTIFIER\\s+::=\\s+\\{\\s*(.*)\\s*}"};
		std::regex_search(id_line, match, tree_regex);
		std::string tree = match[1];
		// std::cout << "Tree: " << tree << std::endl;

		std::vector<std::string> tree_v;
		std::stringstream ss {tree};
		while (std::getline(ss, tree, ' ')) {
			tree_v.push_back(tree);
			// std::cout << tree << std::endl;
		}

		bool in_parent = true;
		std::string parent_name = tree_v[0];
		tree_v.erase(tree_v.begin());
		while (tree_v.size() > 1) {
			// std::regex break_regex{".\\s+\\d+"};
			// if (!std::regex_search(tree, break_regex)) {
			// 	// only the ID number left
			// 	in_parent = false;
			// 	break;
			// }
			
			// iso org(3) dod(6) 1 
			// std::regex get_first_node {"(.*?)\\s+"};
			// std::regex_search(tree, match, get_first_node);
			// std::string first_node = match[1];
			// for (std::string str : tree_v) {
			// 	std::cout << "Tree: " << str << std::endl;
			// }

			std::regex node_id_regex{".*\\((\\d+)\\)"};
			std::regex_search(tree_v[0], match, node_id_regex);
			long node_id = -1;
			std::string node_name;
			if (!match[1].str().empty()) {
				// std::cout << "ID: " << match[1].str() << std::endl;
				node_id = std::stol(match[1]);
				std::regex node_name_regex{"(.+)\\(\\d+\\)"};
				std::regex_search(tree_v[0], match, node_name_regex);
				// std::cout << "Parent and node name: " << match[1] << std::endl;
				node_name = match[1];
			} else {
				// std::regex node_name_regex{"(.+?)\\s+"};
				// std::regex_search(tree, match, node_name_regex);
				// std::cout << "Parent and node name2: " << tree_v[0] << std::endl;
				node_name = tree_v[0];
			}
			//std::cout << "Adding |" << node_name << "| with parent " << parent_name << std::endl;
			node_name.erase(std::remove(node_name.begin(), node_name.end(), ' '), node_name.end());
			parent_name.erase(std::remove(parent_name.begin(), parent_name.end(), ' '), parent_name.end());
			Group_node node_to_put {node_id, node_name, parent_name};
			bool status = root_node.put_node(node_to_put);
			if (!status) {
				std::cerr << "Cannot put node |" << node_name << "| with parent: |" << parent_name << "|" << std::endl;
				throw;
			} else {
				//std::cout << "Put node |" << node_name << "| with parent: |" << parent_name << "|" << std::endl;
			}
			
			parent_name = node_name;
			tree_v.erase(tree_v.begin());
		}

		std::regex parent_id_regex{"(\\d+)"};
		std::regex_search(tree_v[0], match, parent_id_regex);
		long id = std::stoi(match[1]);
		//std::cout << "Adding |" << defined_name << "| with parent " << parent_name << std::endl;
		defined_name.erase(std::remove(defined_name.begin(), defined_name.end(), ' '), defined_name.end());
		parent_name.erase(std::remove(parent_name.begin(), parent_name.end(), ' '), parent_name.end());
		Group_node node_to_put {id, defined_name, parent_name};
		bool status = root_node.put_node(node_to_put);
		if (!status) {
			std::cerr << "Cannot put node |" << defined_name << "| with parent: |" << parent_name << "|" << std::endl;
			throw;
		} else {
				//std::cout << "Put node |" << defined_name << "| with parent: |" << parent_name << "|" << std::endl;
		}
	}	

	static void skip_group(std::stringstream& file, std::string line) {
		// one liner?
	}


	std::string decode(std::vector<int>& oid, uint8_t* ber) {
		std::cout << m_id << ": [" << m_name << "]" << std::endl;
		int id = oid[1];
		oid.erase(oid.begin());
		for (auto& node : m_group_children) {
			if (node.m_id == id) {
				return node.decode(oid, ber);
			}
		}
		for (auto& node : m_object_children) {
			if (node.get_id() == id) {
				return node.decode(ber);
			}
		}
		std::cerr << m_name << ": I don't have children with IDD: |" << id << "|" << std::endl;
		std::cerr << m_name << ": My children: " << std::endl;
		for (auto& node : m_object_children) {
			std::cerr << node.get_id() << ": " << node.get_name() << std::endl;
		}

	}

	std::vector<uint8_t> encode(std::vector<int>& oid, uint8_t* ber, long size) {
		std::cout << m_id << ": [" << m_name << "]" << std::endl;
		int id = oid[1];
		oid.erase(oid.begin());
		for (auto& node : m_group_children) {
				if (node.m_id == id) {
					return node.encode(oid, ber, size);
				}
			}
		for (auto& node : m_object_children) {
				if (node.get_id() == id) {
					return node.encode(ber, size);
				}
			}
		std::cerr << m_name << ": I don't have children with ID: " << id << std::endl;
	}

	std::string getObjectValue(std::vector<int>& oid) const {
		std::cout << m_id << ": [" << m_name << "]" << std::endl;
		int id = oid[1];
		oid.erase(oid.begin());
		for (auto& node : m_group_children) {
			if (node.m_id == id) {
				return node.getObjectValue(oid);
			}
		}
		for (auto& node : m_object_children) {
			if (node.get_id() == id) {
				return node.getObjectValue();
			}
		}
		std::cerr << m_name << ": GETVALUE I don't have children with IDD: |" << id << "|" << std::endl;
		std::cerr << m_name << ": GETVALUE My children: " << std::endl;
		for (auto& node : m_object_children) {
			std::cerr << node.get_id() << ": " << node.get_name() << std::endl;
		}
	}

	void setObjectValue(std::vector<int>& oid, std::string value) {
		std::cout << m_id << ": [" << m_name << "]" << std::endl;
		int id = oid[1];
		oid.erase(oid.begin());
		for (auto& node : m_group_children) {
			if (node.m_id == id) {
				node.setObjectValue(oid, value);
				return;
			}
		}
		for (auto& node : m_object_children) {
			if (node.get_id() == id) {
				node.setObjectValue(value);
				return;
			}
		}
		std::cerr << m_name << ": SETVALUE I don't have children with IDD: |" << id << "|" << std::endl;
		std::cerr << m_name << ": SETVALUE My children: " << std::endl;
		for (auto& node : m_object_children) {
			std::cerr << node.get_id() << ": " << node.get_name() << std::endl;
		}
	}
};

#endif