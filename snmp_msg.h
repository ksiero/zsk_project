#ifndef SNMP_MSG_H
#define SNMP_MSG_H

#include <iostream>
#include <string>
#include <list>

#include "context.h"
#include "file_operation.h"
#include "imports.h"

class Snmp_msg {

	Context global_context {"RFC1213-MIB.txt"};

	int m_id_req = 0;
	int m_operation = 0;
	std::string m_value;
	std::string m_oid_str;

	uint8_t hex2bin(std::string hex) {
		int x;
		std::stringstream ss {hex};
		ss >> std::hex >> x;
		return static_cast<uint8_t>(x);
	}

	int decode_length(const uint8_t* ber, long& len) {
		uint8_t byte = *ber;
		if (byte & 0x80) { // complex
			if (byte & 0x7F) { //fixed size
				std::cout << "Complex Fixed Length" << std::endl;
				uint8_t k = byte & 0x7F;
				std::vector<uint8_t> length;
				for (int i = 0; i < k; i++) {
					length.push_back(*(ber+1 + i));
					std::cout << "Length[" << i << "]: " << (int)length[i] << std::endl;
				}
				if (length.size() > 8) {
					std::cerr << "Didn't exepect that size will be grater that max long";
					throw;
				}
				for (int i = 0; i < length.size(); i++) {
					memcpy(((uint8_t*)&len) + i, &length[length.size()-1 - i], sizeof(uint8_t));
				}
				return length.size() + 1;
			} else { //EOC on end
				std::cout << "Complex EOC Length" << std::endl;
				len = -1;
				return 1;
			}
		} else { //simple
			len = static_cast<size_t>(byte);
			return 1;
		}
	}

	uint8_t* decode_msg(uint8_t* ber) {
		if (ber[0] != 0x30) {
			printf("\033[31mInvalid MSG, SEQ should start with 0x30, but starts with: 0x%x\033[0m\n", ber[0]);
			//std::cerr << "\033[31mInvalid MSG, SEQ should start with 0x30, but starts with: " << static_cast<unsigned char>(ber[0]) << "\033[0m" << std::endl;
			throw;
		}
		ber++;
		long length = 0;
		int offset = decode_length(ber, length);
		std::cout << "SNMP MSG: length = " << length << std::endl;
		return ber + offset;
 	}

	void encode_msg(std::vector<uint8_t>& ber) {
		ber.insert(ber.begin(), static_cast<uint8_t>(ber.size()));
		ber.insert(ber.begin(), 0x30);
	}

	uint8_t* decode_version(uint8_t* ber) {
		if (ber[0] != 0x02) {
			printf("\033[31mInvalid VERSION, should be INT and start with 0x02, but starts with: 0x%x\033[0m\n", ber[0]);
			//std::cerr << "\033[31mInvalid VERSION, should be INT and start with 0x02, but starts with: " << static_cast<unsigned char>(ber[0]) << "\033[0m" << std::endl;
			throw;
		}
		ber++;
		//ber[1] is length = 01
		ber++;
		printf("SNMP MSG: version = 0x%x\n", ber[0]);
		ber++;
		return ber;
 	}

	void encode_version(std::vector<uint8_t>& ber) {
		ber.insert(ber.begin(), 0x00);
		ber.insert(ber.begin(), 0x01);
		ber.insert(ber.begin(), 0x02);
	}

	uint8_t* decode_comm_string(uint8_t* ber) {
		if (ber[0] != 0x04) {
			printf("\033[31mInvalid COMMUNITY STRING, SEQ should start with 0x40, but starts with: 0x%x\033[0m\n", ber[0]);
			//std::cerr << "\033[31mInvalid VERSION, should be INT and start with 0x02, but starts with: " << static_cast<unsigned char>(ber[0]) << "\033[0m" << std::endl;
			throw;
		}
		ber++;
		long length = 0;
		int offset = decode_length(ber, length);
		std::cout << "SNMP COMM STRING: length = " << length << std::endl;
		ber += offset;

		std::cout << "SNMP COMM STRING: 0x";
		std::unique_ptr<uint8_t[]> comm_string { new uint8_t[length] };
		for (int i = 0; i < length; i++) {
			comm_string[i] = ber[i];
			printf("%x", comm_string[i]);
		}
		std::cout << std::endl;
		ber += length;
		return ber;
 	}

	void encode_comm_string(std::vector<uint8_t>& ber) {
		//std::vector<uint8_t> comm_string = {0x70, 0x72, 0x69, 0x76, 0x61, 0x74, 0x65};
		std::vector<uint8_t> comm_string = {0x70, 0x75, 0x62, 0x6c, 0x69, 0x63};
		for (int i = comm_string.size() - 1; i >= 0; i--) {
			ber.insert(ber.begin(), comm_string[i]);
		}
		ber.insert(ber.begin(), static_cast<uint8_t>(comm_string.size()));
		ber.insert(ber.begin(), 0x04);
	}


	uint8_t* decode_PDU(uint8_t* ber) {
		if (ber[0] != 0xA0 && ber[0] != 0xA1 && ber[0] != 0xA2 && ber[0] != 0xA3) {
			printf("\033[31mInvalid PDU, should start with 0xA0 - 0xA3, but starts with: 0x%x\033[0m\n", ber[0]);
			//std::cerr << "\033[31mInvalid VERSION, should be INT and start with 0x02, but starts with: " << static_cast<unsigned char>(ber[0]) << "\033[0m" << std::endl;
			throw;
		}
		if (ber[0] == 0xA0) {
			std::cout << "SNMP PDU: GetRequest" << std::endl;
			m_operation = 0;
		} else if (ber[0] == 0xA1) {
			std::cout << "SNMP PDU: GetNextRequest" << std::endl;
			m_operation = 1;
		} else if (ber[0] == 0xA2) {
			std::cout << "SNMP PDU: Response" << std::endl;
		} else if (ber[0] == 0xA3) {
			std::cout << "SNMP PDU: SetRequest" << std::endl;
			m_operation = 3;
		}
		ber++;
		long length = 0;
		int offset = decode_length(ber, length);
		std::cout << "SNMP PDU: length = " << length << std::endl;
		ber += offset;
		return ber;
 	}

	void encode_PDU(std::vector<uint8_t>& ber, int pdu_type) {
		ber.insert(ber.begin(), static_cast<uint8_t>(ber.size()));
		ber.insert(ber.begin(), static_cast<uint8_t>(0xA0 + pdu_type));
	}


	uint8_t* decode_REQ_ID(uint8_t* ber) {
		if (ber[0] != 0x02) {
			printf("\033[31mInvalid REQ_ID, should be INT and start with 0x02, but starts with: 0x%x\033[0m\n", ber[0]);
			//std::cerr << "\033[31mInvalid VERSION, should be INT and start with 0x02, but starts with: " << static_cast<unsigned char>(ber[0]) << "\033[0m" << std::endl;
			throw;
		}
		ber++;
		long length = 0;
		int offset = decode_length(ber, length);
		std::cout << "SNMP REQ_ID: length = " << length << std::endl;
		ber += offset;
		int value = 0;
		for (int x = 0; x < length; x++) {
			value = value << 8;
			for (int i = 1; i < 1<<9; i = i << 1) {
				value = value | (ber[x] & i);
			}
		}
		m_id_req = value;
		printf("SNMP REQ_ID: value = 0x%x\n", value);
		ber += length;
		return ber;
 	}

	void encode_REQ_ID(std::vector<uint8_t>& ber, int req_id) {
		//ber.insert(ber.begin(), static_cast<uint8_t>(req_id));
		int tmp = req_id;
		int length = 0;
		//VALUE
		while(req_id > 0) {
			uint8_t value = req_id & 0xFF;
			printf("Value: 0x%x\n", value);
			ber.insert(ber.begin(), value);
			req_id = req_id >> 8;
			length++;
		}
		std::cout << "Len: " << length << " req_id = " << req_id << std::endl;
		ber.insert(ber.begin(), static_cast<uint8_t>(length));
		ber.insert(ber.begin(), 0x02);
	}

	uint8_t* decode_REQ_ERROR(uint8_t* ber) {
		if (ber[0] != 0x02) {
			printf("\033[31mInvalid REQ_ERROR, should be INT and start with 0x02, but starts with: 0x%x\033[0m\n", ber[0]);
			//std::cerr << "\033[31mInvalid VERSION, should be INT and start with 0x02, but starts with: " << static_cast<unsigned char>(ber[0]) << "\033[0m" << std::endl;
			throw;
		}
		ber++;
		long length = 0;
		int offset = decode_length(ber, length);
		std::cout << "SNMP REQ_ERROR: length = " << length << std::endl;
		ber += offset;
		int value = 0;
		for (int x = 0; x < length; x++) {
			value = value << 8;
			for (int i = 1; i < 1<<9; i = i << 1) {
				value = value | (ber[x] & i);
			}
		}
		printf("SNMP REQ_ERROR: value = 0x%x\n", value);
		ber += length;
		return ber;
 	}

	void encode_REQ_ERROR(std::vector<uint8_t>& ber) {
		ber.insert(ber.begin(), 0x00);
		ber.insert(ber.begin(), 0x01);
		ber.insert(ber.begin(), 0x02);
	}

	void encode_REQ_ERROR(std::vector<uint8_t>& ber, uint8_t err) {
		ber.insert(ber.begin(), err);
		ber.insert(ber.begin(), 0x01);
		ber.insert(ber.begin(), 0x02);
	}

	uint8_t* decode_REQ_ERROR_IDX(uint8_t* ber) {
		if (ber[0] != 0x02) {
			printf("\033[31mInvalid REQ_ERROR_IDX, should be INT and start with 0x02, but starts with: 0x%x\033[0m\n", ber[0]);
			//std::cerr << "\033[31mInvalid VERSION, should be INT and start with 0x02, but starts with: " << static_cast<unsigned char>(ber[0]) << "\033[0m" << std::endl;
			throw;
		}
		ber++;
		long length = 0;
		int offset = decode_length(ber, length);
		std::cout << "SNMP REQ_ERROR_IDX: length = " << length << std::endl;
		ber += offset;
		int value = 0;
		for (int x = 0; x < length; x++) {
			value = value << 8;
			for (int i = 1; i < 1<<9; i = i << 1) {
				value = value | (ber[x] & i);
			}
		}
		printf("SNMP REQ_ERROR_IDX: value = 0x%x\n", value);
		ber += length;
		return ber;
 	}

	void encode_REQ_ERROR_IDX(std::vector<uint8_t>& ber) {
		ber.insert(ber.begin(), 0x00);
		ber.insert(ber.begin(), 0x01);
		ber.insert(ber.begin(), 0x02);
	}

	void encode_REQ_ERROR_IDX(std::vector<uint8_t>& ber, uint8_t err) {
		ber.insert(ber.begin(), err);
		ber.insert(ber.begin(), 0x01);
		ber.insert(ber.begin(), 0x02);
	}

	uint8_t* decode_varbind_list(uint8_t* ber) {
		if (ber[0] != 0x30) {
			printf("\033[31mInvalid VarBindList, SEQ should start with 0x30, but starts with: 0x%x\033[0m\n", ber[0]);
			//std::cerr << "\033[31mInvalid MSG, SEQ should start with 0x30, but starts with: " << static_cast<unsigned char>(ber[0]) << "\033[0m" << std::endl;
			throw;
		}
		ber++;
		long length = 0;
		int offset = decode_length(ber, length);
		std::cout << "SNMP VarBindList: length = " << length << std::endl;
		return ber + offset;
 	}

	void encode_varbind_list(std::vector<uint8_t>& ber) {
		ber.insert(ber.begin(), static_cast<uint8_t>(ber.size()));
		ber.insert(ber.begin(), static_cast<uint8_t>(0x30));
	}

	uint8_t* decode_varbind(uint8_t* ber) {
		if (ber[0] != 0x30) {
			printf("\033[31mInvalid VarBind, SEQ should start with 0x30, but starts with: 0x%x\033[0m\n", ber[0]);
			//std::cerr << "\033[31mInvalid MSG, SEQ should start with 0x30, but starts with: " << static_cast<unsigned char>(ber[0]) << "\033[0m" << std::endl;
			throw;
		}
		ber++;
		long length = 0;
		int offset = decode_length(ber, length);
		std::cout << "SNMP VarBind: length = " << length << std::endl;
		return ber + offset;
 	}

	void encode_varbind(std::vector<uint8_t>& ber) {
		std::cout << "VARBIND: " << ber.size() << std::endl;
		ber.insert(ber.begin(), static_cast<uint8_t>(ber.size()));
		ber.insert(ber.begin(), static_cast<uint8_t>(0x30));
	}

	uint8_t* decode_OID(uint8_t* ber) {
		if (ber[0] != 0x06) {
			printf("\033[31mInvalid OID, OID should start with 0x06, but starts with: 0x%x\033[0m\n", ber[0]);
			//std::cerr << "\033[31mInvalid MSG, SEQ should start with 0x30, but starts with: " << static_cast<unsigned char>(ber[0]) << "\033[0m" << std::endl;
			throw;
		}
		ber++;
		long length = 0;
		int offset = decode_length(ber, length);
		std::cout << "SNMP OID: length = " << length << std::endl;
		ber += offset;
		std::string oid_str;
		int x = ber[0] / 40;
		int y = ber[0] % 40;
		oid_str = std::to_string(x);
		oid_str += "." + std::to_string(y);
		ber++;
		for (int i = 0; i < length - 1; i++) {
			oid_str += "." + std::to_string(ber[i]);
		}
		std::cout << "SNMP OID: " << oid_str << std::endl;
		ber += length - 1;
		m_oid_str = oid_str;
		return ber;
 	}

	void encode_OID(std::vector<uint8_t>& ber, std::string oid) {
		std::vector<int> oid_vector;
		std::stringstream ss {oid};
		std::string number;
		while(std::getline(ss, number, '.')) {
			oid_vector.push_back(std::stoi(number));
		}
		if (*(oid_vector.end() - 1) != 0) {
			oid_vector.push_back(0);
		}

		for (int i = oid_vector.size() - 1; i > 1; i--) {
			ber.insert(ber.begin(), static_cast<uint8_t>(oid_vector[i]));
		}
		ber.insert(ber.begin(), static_cast<uint8_t>(oid_vector[0] * 40 + oid_vector[1]));
		ber.insert(ber.begin(), static_cast<uint8_t>(oid_vector.size() - 1));
		ber.insert(ber.begin(), 0x06);
	}

public:
	void decode(uint8_t* ber) {
		ber = decode_msg(ber);
		ber = decode_version(ber);
		ber = decode_comm_string(ber);
		ber = decode_PDU(ber);
		ber = decode_REQ_ID(ber);
		ber = decode_REQ_ERROR(ber);
		ber = decode_REQ_ERROR_IDX(ber);
		ber = decode_varbind_list(ber);
		ber = decode_varbind(ber);
		ber = decode_OID(ber);
		m_value = global_context.decode(m_oid_str, ber);
	}

	std::vector<uint8_t> respond(uint8_t* ber) {
		try {
			decode(ber);
		} catch (std::string& node_value) {
			std::cout << "Node value = " << node_value << std::endl;
			if (node_value.size() % 2) {
				node_value = "0" + node_value;
			}
			std::unique_ptr<uint8_t[]> bytes { new uint8_t[node_value.size()/2] };

			for (int i = 0; i < node_value.size()/2; i++) {
				bytes[i] = hex2bin(node_value.substr(i*2, 2));
			}
			
			return encode_with_error(2, m_id_req, m_oid_str, bytes.get(), node_value.size()/2);
		}
		if (m_operation == 0) {
			//get
			std::string node_value = global_context.getObjectValue(m_oid_str);
			if (node_value.size() % 2) {
				node_value = "0" + node_value;
			}
			std::unique_ptr<uint8_t[]> bytes { new uint8_t[node_value.size()/2] };

			for (int i = 0; i < node_value.size()/2; i++) {
				bytes[i] = hex2bin(node_value.substr(i*2, 2));
			}

			std::cout << "Getter: " << node_value << std::endl;
			return encode(2, m_id_req, m_oid_str, bytes.get(), node_value.size()/2);
		} else if (m_operation == 1) {
			//get-next
			std::vector<int> oid_vector;
			std::cout << "OID before: " << m_oid_str << std::endl;
			std::stringstream ss {m_oid_str};
			std::string number;
			while(std::getline(ss, number, '.')) {
				oid_vector.push_back(std::stoi(number));
			}
			(*(oid_vector.end() - 2))++;

			m_oid_str = std::to_string(oid_vector[0]);
			for (int i = 1; i < oid_vector.size(); i++) {
				m_oid_str += "." + std::to_string(oid_vector[i]);
			}
			std::cout << "OID after: " << m_oid_str << std::endl;

			std::string node_value = global_context.getObjectValue(m_oid_str);
			std::unique_ptr<uint8_t[]> bytes { new uint8_t[node_value.size()/2] };

			for (int i = 0; i < node_value.size()/2; i++) {
				bytes[i] = hex2bin(node_value.substr(i*2, 2));
			}

			std::cout << "Getter: " << node_value << std::endl;
			


			return encode(2, m_id_req, m_oid_str, bytes.get(), node_value.size()/2);
		} else if (m_operation == 3) {
			//set
			std::cout << "Setting value: " << m_value << std::endl;
			global_context.setObjectValue(m_oid_str, m_value);
			if (m_value.size() % 2) {
				m_value = "0" + m_value;
			}
			std::unique_ptr<uint8_t[]> bytes { new uint8_t[m_value.size()/2] };

			for (int i = 0; i < m_value.size()/2; i++) {
				bytes[i] = hex2bin(m_value.substr(i*2, 2));
			}
			
			return encode(2, m_id_req, m_oid_str, bytes.get(), m_value.size()/2);
		}
	}

	std::vector<uint8_t> encode_get(int pdu_type, int req_id, std::string oid) {
		std::vector<uint8_t> snmp_msg;
		snmp_msg.push_back(0x05);
		snmp_msg.push_back(0x00);
		encode_OID(snmp_msg, oid);
		encode_varbind(snmp_msg);
		encode_varbind_list(snmp_msg);
		encode_REQ_ERROR_IDX(snmp_msg);
		encode_REQ_ERROR(snmp_msg);
		encode_REQ_ID(snmp_msg, req_id);
		encode_PDU(snmp_msg, pdu_type);
		encode_comm_string(snmp_msg);
		encode_version(snmp_msg);
		encode_msg(snmp_msg);

		// for (auto x : snmp_msg) {
		// 	printf("%02x", x);
		// }
		// std::cout << std::endl;

		return snmp_msg;
	}

	std::vector<uint8_t> encode(int pdu_type, int req_id, std::string oid, uint8_t* value, long value_size) {
		std::vector<uint8_t> snmp_msg = global_context.encode(oid, value, value_size);
		encode_OID(snmp_msg, oid);
		encode_varbind(snmp_msg);
		encode_varbind_list(snmp_msg);
		encode_REQ_ERROR_IDX(snmp_msg);
		encode_REQ_ERROR(snmp_msg);
		encode_REQ_ID(snmp_msg, req_id);
		encode_PDU(snmp_msg, pdu_type);
		encode_comm_string(snmp_msg);
		encode_version(snmp_msg);
		encode_msg(snmp_msg);

		// for (auto x : snmp_msg) {
		// 	printf("%02x", x);
		// }
		// std::cout << std::endl;

		return snmp_msg;
	}

	std::vector<uint8_t> encode_with_error(int pdu_type, int req_id, std::string oid, uint8_t* value, long value_size) {
		std::vector<uint8_t> snmp_msg = global_context.encode(oid, value, value_size);
		encode_OID(snmp_msg, oid);
		encode_varbind(snmp_msg);
		encode_varbind_list(snmp_msg);
		encode_REQ_ERROR_IDX(snmp_msg, 1);
		encode_REQ_ERROR(snmp_msg, 1);
		encode_REQ_ID(snmp_msg, req_id);
		encode_PDU(snmp_msg, pdu_type);
		encode_comm_string(snmp_msg);
		encode_version(snmp_msg);
		encode_msg(snmp_msg);

		// for (auto x : snmp_msg) {
		// 	printf("%02x", x);
		// }
		// std::cout << std::endl;

		return snmp_msg;
	}
};

#endif