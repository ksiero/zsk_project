#include <string>
#include <iostream>
#include "snmp_msg.h"

#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 

int main()
{
    sockaddr_in server_addr, client_addr;
    memset(&client_addr, 0, sizeof(client_addr));
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(161);
    server_addr.sin_addr.s_addr = INADDR_ANY;

    int socket_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (socket_fd < 0){
        perror("Error while openning a socket");
        return 1;
    }

    if (bind(socket_fd, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0) {
        perror("Error while binding a socket");
        close(socket_fd);
        return 1;
    }

    Snmp_msg snmp;
    while (1) {
        socklen_t size = sizeof(client_addr);
        uint8_t buffer[4096];
        std::cout << "Wait for message..." << std::endl;
        auto received_bytes = recvfrom(socket_fd, buffer, sizeof(uint8_t)*4096, 0, (struct sockaddr*) &client_addr, &size);
        if (received_bytes < 0) {
            perror("Cannot receive msg");
            close(socket_fd);
            return 1;
        }

        printf("Received: ");
        for (int i = 0; i < received_bytes; i++) {
            printf("%02x", buffer[i]);
        }
        std::cout << std::endl;

        auto response = snmp.respond(buffer);

        if (sendto(socket_fd, response.data(), response.size(), 0, (struct sockaddr*) &client_addr, size) < 0 ) {
            perror("Cannot send msg");
            close(socket_fd);
            return 1;
        }
    }

    close(socket_fd);
    return 0;
}
