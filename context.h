#ifndef CONTEXT_H
#define CONTEXT_H

#include <fstream>
#include <iostream>
#include <string>
#include <list>

#include "file_operation.h"
#include "imports.h"

class Context {
	std::string m_root_file_path;
	Group_node m_root_node {1, "iso"};
	std::list<Type> m_types;

	void add_predefined_types() {
		m_types.emplace_back("INTEGER", "UNIVERSAL", 2);
		m_types.emplace_back("OCTET STRING", "UNIVERSAL", 4);
		m_types.emplace_back("OBJECT IDENTIFIER", "UNIVERSAL", 6);
		m_types.emplace_back("NULL", "UNIVERSAL", 5);
	}

public:
	Context(std::string path):
		m_root_file_path{path}
	{
		add_predefined_types();

		std::stringstream ss = File_operation::get_file(path);
		Imports::parse_file_types(ss, m_types, m_root_node);

		for (auto& type : m_types) {
			if (type.get_subtype() == nullptr) {
				for (auto& subtype : m_types) {
					if (subtype.get_name() == type.get_subtype_name()) {
						type.set_subtype(&subtype);
						// std::cout << type.get_name() << " has new parent " << subtype.get_name() << std::endl;
					}
				}
			}
		}
		
		// std::cout << "Loaded types:\n";
		// for (auto& type : m_types) {
		// 	type.debug_print();
		// 	std::cout << std::endl << std::endl;
		// }



		// m_root_node.debug_print();

		ss = File_operation::get_file(path);
		Imports::parse_file_nodes(ss, m_root_node, m_types);

		//m_root_node.debug_print();
	}

	std::string decode(std::string oid_str, uint8_t* ber) {
		std::vector<int> oid;
		std::stringstream oid_ss {oid_str};
		while(std::getline(oid_ss, oid_str, '.')) {
			oid.push_back(std::stoi(oid_str));
		}
		return m_root_node.decode(oid, ber);
	}
	std::vector<uint8_t> encode(std::string oid_str, uint8_t* ber, long size) {
		std::vector<int> oid;
		std::stringstream oid_ss {oid_str};
		while(std::getline(oid_ss, oid_str, '.')) {
			oid.push_back(std::stoi(oid_str));
		}
		return m_root_node.encode(oid, ber, size);
	}

	std::string getObjectValue(std::string oid_str) const {
		std::vector<int> oid;
		std::stringstream oid_ss {oid_str};
		while(std::getline(oid_ss, oid_str, '.')) {
			oid.push_back(std::stoi(oid_str));
		}
		return m_root_node.getObjectValue(oid);
	}

	void setObjectValue(std::string oid_str, std::string value) {
		std::vector<int> oid;
		std::stringstream oid_ss {oid_str};
		while(std::getline(oid_ss, oid_str, '.')) {
			oid.push_back(std::stoi(oid_str));
		}
		return m_root_node.setObjectValue(oid, value);
	}
};

#endif