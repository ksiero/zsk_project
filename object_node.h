#ifndef OBJECT_NODE_H
#define OBJECT_NODE_H

#include <algorithm>
#include <cctype>
#include <fstream>
#include <iostream>
#include <list>
#include <regex>
#include <string>
#include <vector>


#include "type.h"

class Object_node {
	std::string object_value;

	long m_id {-1};
	std::string m_name;
	std::string m_parent_name;  // is this necessary?

	long m_min_value {-1};
	long m_max_value {-1};

	long m_min_size {-1};
	long m_max_size {-1};


	const Type* m_syntax {nullptr};
	std::string m_access;
	std::string m_status;
	std::string m_description;

	bool m_sequence_of {false};
	std::vector<std::string> m_seq_index;

	std::vector<std::string> m_valid_values; //possible values and their meaning

public:
	long get_id() const {
		return m_id;
	}
	
	std::string get_name() const {
		return m_name;
	}

	std::string get_parent_name() const {
		return m_parent_name;
	}


	std::string getObjectValue() const {
		return object_value;
	}

	void setObjectValue(std::string value) {
		std::cout << m_name << " value now is " << value << std::endl;
		object_value = value;

		//update file
		std::ofstream myfile;
		myfile.open ("MIB_values.db", std::ios::app);
		myfile << m_name << " " << value << "\n";
		myfile.close();
	}
	

	Object_node(std::stringstream& file, std::string obj_line, const std::list<Type>& types)
	{
		//obj_line - line with OBJECT-TYPE
		std::string line;
		// std::string object_scope = obj_line;
		// bool in_object_scope = true;
		// while (in_object_scope) {
		// 	std::getline(file, line);
		// 	object_scope += line + "\n";  //std::getline removes end of line
		// 	std::regex asign_regex{"::=\\s+\\{.+\\}"};
		// 	if (std::regex_search(object_scope, asign_regex)) {
		// 		in_object_scope = false;
		// 	}
		// }

		std::regex name_regex{"\\s*(.*)\\s+OBJECT-TYPE"};
		std::smatch match;
		std::regex_search(obj_line, match, name_regex);
		m_name = match[1];
		// std::cout << "Name: " << m_name << std::endl;

		std::string whole_syntax;
		std::getline(file, whole_syntax);

		//object members:
		std::regex syntax_line_regex{"SYNTAX\\s+(.*)"};
		std::regex_search(whole_syntax, match, syntax_line_regex);
		std::string syntax_line = match[1];
		//std::cout << "SYNTAX: " << syntax_line  << std::endl;

		//check if sequence_of
		std::regex sequence_of_regex{"SEQUENCE OF"};
		if (std::regex_search(syntax_line, sequence_of_regex)) {
			std::regex remove_sequence_regex{"SEQUENCE OF\\s+(.+)"};
			std::regex_search(syntax_line, match, remove_sequence_regex);
			syntax_line = match[1];
			m_sequence_of = true;
		}

		//check if possible values
		std::regex pos_val_regex{"\\s+\\{"};
		if (std::regex_search(syntax_line, pos_val_regex)) {
			bool in_pos_val = true;
			std::string pos_val_scope = syntax_line;
			line = syntax_line; //first line
			while (in_pos_val) {
				std::regex break_regex {"\\}"};
				if (std::regex_search(line, break_regex)) {
					in_pos_val = false;
					break;
				}
				
				std::getline(file, line);
				// std::cout << "Get posval line: " << line << std::endl;
				pos_val_scope += line + "\n";
			}

			//take type
			std::regex get_type_regex{"(.+)\\s+\\{"};
			std::regex_search(pos_val_scope, match, get_type_regex);
			syntax_line = match[1];
			
			//take only in { }
			std::regex get_def_regex{"\\{((.|\\s)*)\\}"};
			std::regex_search(pos_val_scope, match, get_def_regex);
			pos_val_scope = match[1];
			// std::cout << "Pos val scope: |" << pos_val_scope << "|\n";


			pos_val_scope += ","; //to end of the pos_val
			std::stringstream ss {pos_val_scope};
			while (std::getline(ss, line, ',')) {
				std::regex key_regex {"(.+)\\(\\d+\\)"};
				std::regex_search(line, match, key_regex);
				std::regex space_regex{"\\s+"};
				std::string key = std::regex_replace(match[1].str(), space_regex, "");
				//std::cout << "Key: " << key << std::endl;
				m_valid_values.emplace_back(std::move(key));

				// std::regex value_regex {".+\\((\\d+)\\)"};
				// std::regex_search(line, match, value_regex);
				// std::string value  = std::regex_replace(match[1].str(), space_regex, "");
				// std::cout << "Value: " << value << std::endl;
				
			}
		}

		
		


		//check if there is a size
		int is_size = 0;

		std::regex size_regex{"SIZE"};//\\s*\\(\\s*(\\d+)\\.\\."};
		if (std::regex_search(syntax_line, size_regex)) {
			std::regex min_regex{"\\(\\s*(\\d+)\\.\\."};
			std::regex_search(syntax_line, match, min_regex);
			m_min_size = std::stoi(match[1]);
			//std::cout << m_name  << " Min size: " << m_min_size << std::endl;

			std::regex max_regex{"\\(\\s*\\d+\\.\\.(\\d+)"};
			std::regex_search(syntax_line, match, max_regex);
			m_max_size = std::stoi(match[1]);

			//std::cout << m_name << " Max size: " << m_max_size << std::endl;
			is_size = 2;
		} else {
			std::regex min_regex{"\\(\\s*(\\d+)\\.\\."};
			std::regex_search(syntax_line, match, min_regex);
			if (!match[1].str().empty()) {
				m_min_value = std::stoi(match[1]);
				//std::cout << m_name << " Min val: " << m_min_value << std::endl;

				std::regex max_regex{"\\(\\s*\\d+\\.\\.(\\d+)"};
				std::regex_search(syntax_line, match, max_regex);
				m_max_value = std::stoi(match[1]);
				//std::cout << m_name << " Max val: " << m_max_value << std::endl;
				is_size = 1;
			}
		}

		

    //SYNTAX  INTEGER (0..65535)
    //SYNTAX  DisplayString (SIZE (0..255))
		std::string syntax_type;
		if (is_size == 1) {
			// size with .. SYNTAX: INTEGER (0..65535)
			/// std::cout << "Syntax ..: " << syntax_line << std::endl;
			std::regex syntax_regex{"(.+)\\s+\\(\\s*\\d+\\.\\."};
			std::regex_search(syntax_line, match, syntax_regex);
			// std::cout << "Syntax .. type: " << match[1].str() << std::endl;
			
			syntax_type = match[1];
		} else if (is_size == 2) {			
			// size with SIZE()
			std::regex syntax_regex{"(.+)\\s+\\(\\s*SIZE"};
			std::regex_search(syntax_line, match, syntax_regex);
			// std::cout << "Syntax SIZE type: " << match[1].str() << std::endl;
			
			syntax_type = match[1];
		} else {
			//no size
			// std::regex syntax_regex{"(.+)\\s+\\(\\s*SIZE"};
			// std::regex_search(syntax_line, match, syntax_regex);
			// std::cout << "Syntax type: " << syntax_line << std::endl;
			
			syntax_type = syntax_line;
		}


		// std::regex syntax_regex{"(.*?)\\s*"};
		// std::regex_search(syntax_line, match, syntax_regex);
		// std::string type_str = match[1].str();
		for (const auto& type : types) {
			if (syntax_type == type.name()) {
				// std::cout << "Type :D : " << type.name() << std::endl;
				m_syntax = &type;
			}
		}
		if (m_syntax == nullptr) {
			std::cerr << "SYNTAX IS NOT THE TYPE! " << syntax_type << std::endl;
			throw;
		}

		// std::cout << std::endl;

		//end of parsing SYNTAX ***************************************

		bool in_object_scope = true;
		std::string object_scope;
		while (in_object_scope) {
			std::getline(file, line);
			object_scope += line + "\n";  //std::getline removes end of line
			std::regex asign_regex{"::=\\s+\\{.+\\}"};
			if (std::regex_search(object_scope, asign_regex)) {
				in_object_scope = false;
			}
		}
		
		std::regex parent_regex{"::=\\s+\\{\\s*(.*)\\s*}"};
		std::regex_search(object_scope, match, parent_regex);

		std::string parent_pair = match[1];
		std::smatch parent_match;
		std::regex parent_name_regex{"(.*)\\s+\\d+"};
		std::regex_search(parent_pair, parent_match, parent_name_regex);
		m_parent_name = parent_match[1];

		std::regex parent_id_regex{".*\\s+(\\d+)"};
		std::regex_search(parent_pair, parent_match, parent_id_regex);
		m_id = std::stoi(parent_match[1]);
		


		

		std::regex access_regex{"ACCESS\\s+(.*)"};
		std::regex_search(object_scope, match, access_regex);
		m_access = match[1];

		std::regex status_regex{"STATUS\\s+(.*)"};
		std::regex_search(object_scope, match, status_regex);
		m_status = match[1];

		std::regex description_regex{"DESCRIPTION\\s+\"((.|\n)*)\""};
		std::regex_search(object_scope, match, description_regex);
		m_description = match[1];

		//delete whitespaces
		m_description.erase(std::remove(m_description.begin(), m_description.end(), '\n'), m_description.end());
		m_description.erase(std::remove(m_description.begin(), m_description.end(), '\r'), m_description.end());
		std::regex space_regex{"\\s+"};
		m_description = std::regex_replace(m_description, space_regex, " ");

		//INDEX IN CASE OF SEQUENCE
		/*
			INDEX   { tcpConnLocalAddress,
						tcpConnLocalPort,
						tcpConnRemAddress,
						tcpConnRemPort }
		*/
		std::regex index_regex{"INDEX\\s+((.|\n)*)"};
		std::regex_search(object_scope, match, index_regex);
		std::string index_scope_max = match[1];
		if (index_scope_max.size()) {
			bool in_index = true;
			std::stringstream ss {index_scope_max};
			std::string index_scope;
			std::getline(ss, index_scope);
			
			std::regex break_regex {"\\}"};
			if (std::regex_search(index_scope, break_regex)) {
				in_index = false;
			}
			//std::cout << "in_index: " << in_index << std::endl;
			while (in_index) {
				std::getline(ss, line);
				// std::cout << "Get posval line: " << line << std::endl;
				index_scope += line;
				//std::cout<< "index_scope:\n" << index_scope <<std::endl;
				if (std::regex_search(line, break_regex)) {
					in_index = false;
					break;
				}
			}
			
			//take only in { }
			std::regex get_def_regex{"\\{((.|\\s)*)\\}"};
			std::regex_search(index_scope, match, get_def_regex);
			index_scope = match[1];


			index_scope += ","; //to end of the index_scope
			std::stringstream ss2 {index_scope};
			while (std::getline(ss2, line, ',')) {
				std::regex space_regex{"\\s+"};
				std::string index_elem = std::regex_replace(line, space_regex, "");
				//std::cout << "Index_elem: " << index_elem << std::endl;
				m_seq_index.emplace_back(std::move(index_elem));
				//std::cout << "Seq size: " << m_seq_index.size() << std::endl;
			}
		}

		//load value
		//update valuee
		std::ifstream myfile;
		myfile.open ("MIB_values.db");
		std::string key_value;
		while (std::getline(myfile, key_value)) {
			std::stringstream ss {key_value};
			std::string key;
			std::getline(ss, key, ' ');
			if (key == m_name) {
				std::string value;
				std::getline(ss, value);
				object_value = value;
			}
		}
		myfile.close();
	}

	void debug_print() const {
		std::cout <<  m_name << ":\n\tID: " << m_id << "\n\tParent: " << m_parent_name << std::endl;
		std::cout << "\tSYNTAX: " << m_syntax->name() << std::endl;
		if (m_min_value != -1) {
			std::cout << "\t\tMin value: " << m_min_value << "\n\t\tMax value: " << m_max_value << std::endl;
		}
		if (m_min_size != -1) {
			std::cout << "\t\tMin size: " << m_min_size << "\n\t\tMax size: " << m_max_size << std::endl;
		}
		if (m_sequence_of || m_syntax->is_seq()) {
			std::cout << "\t\tSEQUENCE " << m_seq_index.size() << std::endl;
			if (m_seq_index.size()) {
				std::cout << "\t\tSEQUENCE INDEXES" << std::endl;
				for (auto index : m_seq_index) {
					std::cout << "\t\t\t" << index << std::endl;
				}
			}
		}
		if (m_valid_values.size()) {
			std::cout << "\t\tValid values:" << std::endl;
			int i = 1;
			for (const auto& value : m_valid_values) {
				std::cout << "\t\t  " << value << "(" << i << ")" << std::endl;
				i++;
			}
		}

		std::cout << "\tACCESS: " << m_access<< "\n\tSTATUS: " << m_status << std::endl;
		std::cout << "\tDESCRIPTION: " << m_description << std::endl << std::endl;
		
	}

	std::string decode(uint8_t* ber) {
		std::cout << m_id << ": \e[1m" << m_name << "\e[0m" << std::endl;
		std::cout << "TAG: " << std::endl;
		int vis = 0;
		bool PC = 0;
		int tag = 0;
		int offset = decode_tag(ber, vis, PC, tag);

		int make_a_throw = 0;
		std::stringstream error_name;
		if (PC) {
			error_name << "\033[31mInvalid PC: " << PC << " || " << m_name << " doesn't have IMPLICIT type" << "\033[0m" << std::endl;
			make_a_throw = 1;
		}
		if (m_syntax->get_visibility() != vis) {
			error_name << "\033[31mInvalid CLASS: " << vis << " || " << m_name << " class is " << m_syntax->get_visibility() << "\033[0m" << std::endl;
			make_a_throw = 1;
		}
		if (tag == 5) {
			std::cout << "\033[32mValue: NULL\033[0m" << std::endl;
			return "";
		}
		if (m_syntax->get_tag() != tag) {
			std::cerr << "\033[31mInvalid TAG: " << tag << " || " << m_name << " tag is " << m_syntax->get_tag() << "\033[0m" << std::endl;
			make_a_throw = 1;
		}

		std::cout << "LEN: " << std::endl;
		long length = 0;
		offset += decode_length(ber + offset, length);
		std::cout << "Length: " << length << std::endl;
		if (make_a_throw) {
			std::cerr << error_name.str();
			std::stringstream to_throw;
			for (int x = 0; x < length; x++) {
				to_throw << std::hex << (ber[offset + x]);
			}
			throw std::string{to_throw.str()};
		}
		if (m_syntax->get_size() != -1 && length != m_syntax->get_size()) {
			std::cerr << "\033[31mInvalid SIZE: " << length << " || " << m_name << " size is " << m_syntax->get_size() << "\033[0m" << std::endl;
			std::stringstream to_throw;
			for (int x = 0; x < length; x++) {
				to_throw << std::hex << (ber[offset + x]);
			}
			throw std::string{to_throw.str()};
		}
		if (m_min_size != -1 && (length < m_min_size || length > m_max_size) ) {
			std::cerr << "\033[31mInvalid SIZE: " << length << " || " << m_name << " size is between " << m_min_size << " and " << m_max_size << "\033[0m" << std::endl;
			std::stringstream to_throw;
			for (int x = 0; x < length; x++) {
				to_throw << std::hex << (ber[offset + x]);
			}
			throw std::string{to_throw.str()};
		}

		if (m_syntax->get_base_type().find("INTEGER") != std::string::npos) {
			bool minus = false;
			if (ber[offset] & 0x80) {
				minus = true;
				//make positive
				for (int x = 0; x < length; x++) {
					ber[offset + x] = ber[offset + x] ^ 0xFF;
				}
			}


			int value = 0;
			for (int x = 0; x < length; x++) {
				value = value << 8;
				for (int i = 1; i < 1<<9; i = i << 1) {
					value = value | (ber[offset + x] & i);
				}
			}

			if (minus) {
				//make negative
				value = value ^ 0xFFFFFFFF;
			}

			auto min_max = m_syntax->get_min_max();
			if (min_max.first != -1 && (value < min_max.first || value > min_max.second)) {
				std::cerr << "\033[31mInvalid integer VALUE: " << value << " || " << m_name << " value is between " << min_max.first << " and " << min_max.second << "\033[0m" << std::endl;
				std::stringstream to_throw;
				for (int x = 0; x < length; x++) {
					to_throw << std::hex << (ber[offset + x] ^ 0xFF);
				}
				throw std::string{to_throw.str()};
			}

			if (m_min_value != -1 && (value < m_min_value || value > m_max_value)) {
				std::cerr << "\033[31mInvalid integer VALUE: " << value << " || " << m_name << " value is between " << m_min_value << " and " << m_max_value << "\033[0m" << std::endl;
				std::stringstream to_throw;
				for (int x = 0; x < length; x++) {
					to_throw << std::hex << (ber[offset + x] ^ 0xFF);
				}
				throw std::string{to_throw.str()};
				throw;
			}

			std::cout << "\033[32mInteger value: " << value << "\033[0m" << std::endl;
			std::stringstream stream;
			stream << std::hex << value;
			return stream.str();
		} else if (m_syntax->get_base_type().find("OCTET STRING") != std::string::npos) {
			std::cout << "\033[32mOctet string: ";
			
			for (int x = 0; x < length; x++) {
				printf("%02x ", (int)ber[offset + x]);
			}
			std::cout << "\033[0m" << std::endl;
			std::stringstream stream;
			for (int x = 0; x < length; x++) {
				stream << std::hex << (int)ber[offset + x];
			}
			return stream.str();
		} else if (m_syntax->get_base_type().find("NULL") != std::string::npos) {
			std::cout << "Type null. This is not an error" << std::endl;
			return "";
		} else {
			std::cerr << "\033[31m" << m_syntax->get_base_type() << " is not supported" << std::endl;
			throw;
		}
	}

	int decode_tag(const uint8_t* ber, int& vis, bool& PC, int&tag) {
		uint8_t byte = *ber;
		vis = byte >> 6;
		std::cout << "Class: " << vis << std::endl;

		byte = *ber;
		PC = byte = (byte & 0x20) >> 5;
		std::cout << "PC: " << PC << std::endl;

		byte = *ber;
		byte = (byte & 0x1F);
		if (byte == 0x1F) {
			std::vector<uint8_t> ber_tag_v;
			int i = 1;
			while (1) {
				byte = *(ber + i);
				uint8_t byte_to_add = byte & 0x7F;
				ber_tag_v.push_back(byte_to_add);
				if ( !(byte & 0x80) ) {
					break;
				}
				i++;
			}
			tag = 0;
			for (auto byte_el : ber_tag_v) {
				tag = tag << 7;
				tag = tag | byte_el;
			}
			std::cout << "Complex tag number: " << tag << std::endl;
			return ber_tag_v.size() + 1;
		} else {
			tag = static_cast<int>(byte);
			std::cout << "Tag number: " << tag << std::endl;
			return 1;
		}
	}

	int decode_length(const uint8_t* ber, long& len) {
		uint8_t byte = *ber;
		if (byte & 0x80) { // complex
			if (byte & 0x7F) { //fixed size
				std::cout << "Complex Fixed Length" << std::endl;
				uint8_t k = byte & 0x7F;
				std::vector<uint8_t> length;
				for (int i = 0; i < k; i++) {
					length.push_back(*(ber+1 + i));
					std::cout << "Length[" << i << "]: " << (int)length[i] << std::endl;
				}
				if (length.size() > 8) {
					std::cerr << "Didn't exepect that size will be grater that max long";
					throw;
				}
				for (int i = 0; i < length.size(); i++) {
					memcpy(((uint8_t*)&len) + i, &length[length.size()-1 - i], sizeof(uint8_t));
				}
				return length.size() + 1;
			} else { //EOC on end
				std::cout << "Complex EOC Length" << std::endl;
				// std::vector<uint8_t> length;
				// int i = 1;
				// bool last_was_0 = false;
				// while (1) {
				// 	byte = *(ber - i);
				// 	std::cout << "Byte: " << (int)byte << std::endl;
				// 	if (byte == 0) {
				// 		if (last_was_0 == true) {
				// 			length.pop_back();
				// 			break;
				// 		}
				// 		last_was_0 = true;
				// 	}
				// 	length.push_back(byte);
				// 	i++;
				// }
				// for (auto x : length) {
				// 	std::cout << "Length: " << (int)x << std::endl;
				// }
				len = -1;
				return 1;
			}
		} else { //simple
			std::cout << "Simple Length: " << (int)byte << std::endl;
			len = static_cast<size_t>(byte);
			return 1;
		}
	}

	std::vector<uint8_t> encode(const uint8_t* bin, long size) {
		std::cout << m_id << ": \e[1m" << m_name << "\e[0m" << std::endl;
		std::vector<uint8_t> bytes;
		//ID
		uint8_t vis = static_cast<uint8_t>(m_syntax->get_visibility());
		uint8_t PC = 0;
		
		if (m_syntax->get_tag() < 1<<5) {
			uint8_t tag = static_cast<uint8_t>(m_syntax->get_tag());
			uint8_t id_octet = 0;
			id_octet = id_octet | (vis << 6);
			id_octet = id_octet | (PC << 5);
			id_octet = id_octet | tag;
			bytes.push_back(id_octet);
		} else {
			uint8_t id_octet = 0;
			id_octet = id_octet | (vis << 6);
			id_octet = id_octet | (PC << 5);
			id_octet = id_octet | 0x00011111;
			bytes.push_back(id_octet);

			int tag = m_syntax->get_tag();
			uint8_t to_push = 0; 
			if (tag < 1<<8) {
				//1 byte
				to_push = static_cast<uint8_t>(tag);
				bytes.push_back(to_push);
			} else if (tag < 1<<15) {
				//2 bytes
				to_push = 0x80;
				to_push = to_push | (tag && 0x3F80);
				bytes.push_back(to_push);
				to_push = 0;
				to_push = to_push | (tag && 0x007F);
				bytes.push_back(to_push);
			} else if (tag < 1<<22) {
				//3 bytes
				to_push = 0x80;
				to_push = to_push | (tag && 0x1FC000);
				bytes.push_back(to_push);
				to_push = 0x80;
				to_push = to_push | (tag && 0x003F80);
				bytes.push_back(to_push);
				to_push = 0;
				to_push = to_push | (tag && 0x00007F);
				bytes.push_back(to_push);
			} else if (tag < 1<<29) {
				//4 bytes
				to_push = 0x80;
				to_push = to_push | (tag && 0x0FE00000);
				bytes.push_back(to_push);
				to_push = 0x80;
				to_push = to_push | (tag && 0x001FC000);
				bytes.push_back(to_push);
				to_push = 0x80;
				to_push = to_push | (tag && 0x00003F80);
				bytes.push_back(to_push);
				to_push = 0;
				to_push = to_push | (tag && 0x0000007F);
				bytes.push_back(to_push);
			} else {
				//5 bytes
				to_push = 0x80;
				to_push = to_push | (tag && 0xF0000000);
				bytes.push_back(to_push);
				to_push = 0x80;
				to_push = to_push | (tag && 0x0FE00000);
				bytes.push_back(to_push);
				to_push = 0x80;
				to_push = to_push | (tag && 0x001FC000);
				bytes.push_back(to_push);
				to_push = 0x80;
				to_push = to_push | (tag && 0x00003F80);
				bytes.push_back(to_push);
				to_push = 0;
				to_push = to_push | (tag && 0x0000007F);
				bytes.push_back(to_push);

			}
		}

		//SIZE
		if (size < 128) {
			uint8_t len_octet = 0;
			len_octet = static_cast<uint8_t>(size);
			bytes.push_back(len_octet);
		} else {
			int k;
			if (size % 255 == 0) {
				k = size/255;
			} else {
				k = size/255 + 1;
			}
			uint8_t len_octet = k;
			len_octet = len_octet | 0x80;
			bytes.push_back(len_octet);

			for (int i = k-1; i >= 0; i--) {
				long size_tmp = size;
				size_tmp = size_tmp >> 8*i;
				size_tmp = size_tmp & 0xFF;
				std::cout << size << " " << size_tmp << std::endl;
				uint8_t to_push = static_cast<uint8_t>(size_tmp);
				bytes.push_back(to_push);
			}
		}

		//VALUE
		for (int i = 0; i < size; i++) {
			bytes.push_back(bin[i]);
		}

		// for (auto x : bytes) {
		// 	printf("%02x ", x);
		// }
		// std::cout << std::endl;
		return bytes;
	}

	static void skip_object_node(std::stringstream& file, std::string obj_line)
	{
		//obj_line - line with OBJECT-TYPE
		std::string line;
		std::string object_scope = obj_line;
		bool in_object_scope = true;
		while (in_object_scope) {
			std::getline(file, line);
			object_scope += line + "\n";  //std::getline removes end of line
			std::regex asign_regex{"::=\\s+\\{.+\\}"};
			if (std::regex_search(object_scope, asign_regex)) {
				in_object_scope = false;
			}
		}
	}
};

#endif