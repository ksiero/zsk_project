#ifndef TYPE_H
#define TYPE_H

#include <iostream>
#include <fstream>
#include <list>
#include <map>
#include <string>
#include <vector>



class Type {
	std::string m_name;
	std::string m_subtype_name;
	Type* m_subtype {nullptr};
	std::vector<Type> m_sequence;

	long m_min_value {-1};
	long m_max_value {-1};

	long m_size {-1};

	std::string m_visibility;
	long m_visibility_id {-1};

public:
	std::string get_base_type() const {
		if (m_subtype) {
			return m_subtype->get_base_type();
		} else {
			return m_name;
		}
	}
	
	int get_visibility() const {
		if (m_visibility.empty()) {
			return m_subtype->get_visibility();
		} else {
			if (m_visibility == "UNIVERSAL")
				return 0;
			if (m_visibility == "APPLICATION")
				return 1;
			if (m_visibility == "CONTEXT-SPECIFIC")
				return 2;
			if (m_visibility == "PRIVATE")
				return 3;
		}
	}

	int get_tag() const {
		if (m_visibility_id == -1) {
			return m_subtype->get_tag();
		} else {
			return m_visibility_id;
		}
	}

	Type* get_subtype() const {
		return m_subtype;
	}

	void set_subtype(Type* type_ptr) {
		m_subtype = type_ptr;
	}


	int get_size() const {
		return m_size;
	}

	std::string get_name() const {
		return m_name;
	}

	std::string get_subtype_name() const {
		return m_subtype_name;
	}


	std::pair<long, long> get_min_max() const {
		if (m_min_value == -1) {
			if (m_subtype) {
				return {m_subtype->m_min_value, m_subtype->m_max_value};
			} else {
				return {-1, -1};
			}
		} else {
			return {m_min_value, m_max_value};
		}
	}

	Type()
	{}

	Type(std::string name)
	{
		m_name = std::move(name);
	}

	Type(std::string name, std::string vis, int tag)
	{
		m_name = std::move(name);
		m_visibility = vis;
		m_visibility_id = tag;
	}

	Type(long size, std::string name)
	{
		m_size = size;
		m_name = std::move(name);
	}

	Type(long min, long max, std::string name)
	{
		m_min_value = min;
		m_max_value = max;
		m_name = std::move(name);
	}

	Type(std::stringstream& file, std::string type_line, std::list<Type>& types)
	{
		std::regex name_regex{"\\s*(.+)\\s+::="};
		std::smatch match;
		std::regex_search(type_line, match, name_regex);
		m_name = match[1];

		// std::cout << "\t\t(Type) Name: " << m_name << std::endl;

		std::regex derive_regex{"::=\\s*(.*)"};
		std::smatch derive_match;
		std::regex_search(type_line, derive_match, derive_regex);
		if (derive_match[1].str().empty()) {
			// std::cout << "\t\t(Type) Nothing more in this line, get next" << std::endl;
			//get next line
			std::string next_line;
			std::getline(file, next_line);
			type_line += next_line;
		}

		std::regex visibility_regex{"\\[.+\\s+\\d+\\]"};
		if (std::regex_search(type_line, visibility_regex)) {
			// std::cout << "\t\t(Type) Found visibility in |" << type_line << "|" << std::endl;
			//visibility
			std::regex vis_name_regex {"\\[(.+)\\s+\\d+\\]"};
			std::regex_search(type_line, match, vis_name_regex);
			m_visibility = match[1];

			// std::cout << "\t\t(Type) Visibility: " << m_visibility << std::endl;

			std::regex vis_id_regex {"\\[.+\\s+(\\d+)\\]"};
			std::regex_search(type_line, match, vis_id_regex);
			// std::cout << "\t\t(Type) Vis_id: " << match[1].str() << std::endl;
			m_visibility_id = std::stol(match[1]);
			
			std::string next_line;
			std::getline(file, next_line);
			type_line += next_line;
		}

		std::regex seq_regex {"SEQUENCE|CHOICE"};
		if (std::regex_search(type_line, seq_regex)) {
			//sequence
			bool in_seq = true;
			while (in_seq) {
				std::string whole_line;

				bool in_member = true;
				while (in_member) {
					std::string line;
					getline(file, line);
					whole_line += line + "\n";

					std::regex break_regex{"\\s*(,|})"};
					if (std::regex_search(whole_line, break_regex)) {
						in_member = false;
						// std::cout << "Whole line: " << whole_line << std::endl;
						std::regex key_regex{"\\s*(.+)\\s+.+"};
						std::regex_search(whole_line, match, key_regex);
						// std::cout << "\t\t(Type) Seq key: " << match[1].str() << std::endl;
						std::string key = match[1];

						std::regex value_regex{".+\\s+(\\w+)"};
						std::regex_search(whole_line, match, value_regex);
						// std::cout << "\t\t(Type) Seq value: " << match[1].str() << std::endl;
						std::string value = match[1];
						if (value[value.size()-1] == ',') {
							value = value.substr(0, value.size()-1);
						}

						//check if there is the size
						std::regex value2_regex{"\\(\\d+\\.\\.\\d+\\)"};
						std::regex size_regex{"SIZE\\s*\\(\\d+\\)"};
						if (std::regex_search(value, value2_regex)) {
							std::regex min_regex{"\\((\\d+)\\.\\.\\d+\\)"};
							std::regex_search(value, match, min_regex);
							// std::cout << "\t\t(Type) Seq Min: " << match[1].str() << std::endl;
							long min = std::stol(match[1]);

							std::regex max_regex{"\\(\\d+\\.\\.(\\d+)\\)"};
							std::regex_search(value, match, max_regex);
							// std::cout << "\t\t(Type) Seq Max: " << match[1].str() << std::endl;
							long max = std::stol(match[1]);

							std::string parent;
							std::regex subname_regex{"(.+)\\s+\\(\\d+\\.\\.\\d+\\)"};
							std::regex_search(value, match, subname_regex);
							// std::cout << "\t\t(Type) Seq subname: " << match[1] << std::endl;
							parent = match[1];
							m_subtype_name = parent;

							m_subtype_name = parent;
							Type* seq_el_subtype;
							bool added = false;
							for (auto& t : types) {
								if (t.m_name == parent) {
									seq_el_subtype = &t;
									added = true;
									break;
								}
							}
							if (!added) {
								// std::cerr << key << " has no PARENT (" << parent << ") " << m_subtype << std::endl;
							} else {
								// std::cerr << key << " has parent (" << parent << ")" << m_subtype << std::endl;
							}

							Type type_to_put {min, max, key};
							type_to_put.m_subtype = seq_el_subtype;
							m_sequence.emplace_back(std::move(type_to_put));
							//m_sequence.emplace_back(std::pair<std::string, Type>{std::move(key), Type(min, max, value)});
						} else if (std::regex_search(value, size_regex)) {
							std::regex min_regex{"SIZE\\s*\\((\\d+)\\)"};
							std::regex_search(value, match, min_regex);
							// std::cout << "\t\t(Type) Seq2 min and max: " << match[1].str() << std::endl;
							long size = std::stol(match[1]);

							std::string parent;
							std::regex subname_regex{"(.+)\\s+SIZE\\s*\\((\\d+)\\)"};
							std::regex_search(value, match, subname_regex);
							// std::cout << "\t\t(Type) Seq2 subname: " << match[1] << std::endl;
							parent = match[1];
							m_subtype_name = parent;

							Type* seq_el_subtype;
							bool added = false;
							for (auto& t : types) {
								if (t.m_name == parent) {
									seq_el_subtype = &t;
									added = true;
									break;
								}
							}
							if (!added) {
								// std::cerr << key << " has no PARENT (" << parent << ") " << m_subtype << std::endl;
							} else {
								// std::cerr << key << " has parent (" << parent << ")" << m_subtype << std::endl;
							}
							Type type_to_put {size, key};
							type_to_put.m_subtype = seq_el_subtype;
							m_sequence.emplace_back(std::move(type_to_put));
							//m_sequence.emplace_back(std::pair<std::string, Type>{std::move(key), Type(min, max, value)});
						} else {
							// std::cout << "\t\t(Type) Seq without size: " << key << " " << value << std::endl;
							m_subtype_name = value;
							Type* seq_el_subtype;
							bool added = false;
							for (auto& t : types) {
								if (t.m_name == value) {
									seq_el_subtype = &t;
									added = true;
									break;
								}
							}
							if (!added) {
								// std::cerr << key << " has no PARENT (" << value << ") " << m_subtype << std::endl;
							} else {
								// std::cerr << key << " has parent (" << value << ")" << m_subtype << std::endl;
							}

							Type type_to_put {key};
							type_to_put.m_subtype = seq_el_subtype; //parent
							m_sequence.emplace_back(std::move(type_to_put));
							//m_sequence.emplace_back(std::pair<std::string, Type>{std::move(key), Type(value)});
						}
					}
				}

				std::regex break_regex{"\\}"};
				if (std::regex_search(whole_line, break_regex)) {
					in_seq = false;
				}

			}

		} else {
			//check if there is the size
			std::regex value2_regex{"\\(\\d+\\.\\.\\d+\\)"};
			std::regex size_regex{"SIZE\\s*\\(\\d+\\)"};
			if (std::regex_search(type_line, value2_regex)) {
				std::regex min_regex{"\\((\\d+)\\.\\.\\d+\\)"};
				std::regex_search(type_line, match, min_regex);
				// std::cout << "\t\t(Type) Min: " << match[1].str() << std::endl;
				long min = std::stol(match[1]);

				std::regex max_regex{"\\(\\d+\\.\\.(\\d+)\\)"};
				std::regex_search(type_line, match, max_regex);
				// std::cout << "\t\t(Type) Max: " << match[1].str() << std::endl;
				long max = std::stol(match[1]);

				//try with visibility
				std::regex subname_regex{"\\[.+\\s+\\d+\\]\\s+(.+)\\s+\\(\\d+\\.\\.\\d+\\)"};
				std::regex_search(type_line, match, subname_regex);
				if (match[1].str().empty()) {
					std::regex subname_regex{"(.+)\\s+\\(\\d+\\.\\.\\d+\\)"};
					std::regex_search(type_line, match, subname_regex);
				}
				std::string value = match[1];
				std::regex implicit_regex{"IMPLICIT\\s+"};
				value = std::regex_replace(value, implicit_regex, "");
				// std::cout << "\t\t(Type) Subname: " << match[1].str() << std::endl;
				m_subtype_name = value;
				m_min_value = min;
				m_max_value = max;
				bool added = false;
				for (auto& t : types) {
					if (t.m_name == value) {
						m_subtype = &t;
						added = true;
						break;
					}
				}
				if (!added) {
					// std::cerr << m_name << " has no PARENT (" << value << ")" << std::endl;
				} else {
					// std::cerr << m_name << " has parent (" << value << ")" << std::endl;
				}
			} else if (std::regex_search(type_line, size_regex)) {
				std::regex min_regex{"SIZE\\s*\\((\\d+)\\)"};
				std::regex_search(type_line, match, min_regex);
				// std::cout << "\t\t(Type) Min and Max: " << match[1].str() << std::endl;
				long size = std::stol(match[1]);

				//try with visibility
				std::regex subname_regex{"\\[.+\\s+\\d+\\]\\s+(.+)\\s+\\(\\s*SIZE\\s*\\(\\d+\\)"};
				std::regex_search(type_line, match, subname_regex);
				if (match[1].str().empty()) {
					//there is no visibility
					std::regex subname_regex{"\\s*(.+)\\s+\\(\\s*SIZE\\s*\\(\\d+\\)"};
					std::regex_search(type_line, match, subname_regex);
				}
				std::string value = match[1];
				std::regex implicit_regex{"IMPLICIT\\s+"};
				value = std::regex_replace(value, implicit_regex, "");
				// std::cout << "\t\t(Type) Subname: " << match[1].str() << std::endl;
				m_subtype_name = value;
				bool added = false;
				m_size = size;
				for (auto& t : types) {
					if (t.m_name == value) {
						m_subtype = &t;
						added = true;
						break;
					}
				}
				if (!added) {
					// std::cerr << m_name << " has no PARENT (" << value << ")" << std::endl;
				} else {
					// std::cerr << m_name << " has parent (" << value << ")" << std::endl;
				}
			} else {
				//try with visibility
				std::regex subname_regex{"::=\\s+\\[.+\\s+\\d+\\]\\s+(.+)\\s*"};
				std::regex_search(type_line, match, subname_regex);
				if (match[1].str().empty()) {
					//there is no visibility
					std::regex parent_name_regex {"::=\\s+(.+)\\s*"};
					std::regex_search(type_line, match, parent_name_regex);
				}
				std::regex space_regex{"\\s+"};
				std::string subtype_name = std::regex_replace(match[1].str(), space_regex, " ");
				std::regex implicit_regex{"IMPLICIT\\s+"};
				subtype_name = std::regex_replace(subtype_name, implicit_regex, "");
				// std::cout << "\t\t(" << m_name << ") Subname ((SIMPLE)): |" << subtype_name << "|" << std::endl;
				m_subtype_name = subtype_name;
				bool added = false;
				for (auto& t : types) {
					if (t.m_name == subtype_name) {
						added = true;
						m_subtype = &t;
						break;
					}
				}
				if (!added) {
					// std::cout << m_name << " has no PARENT (" << subtype_name << ") " << m_subtype << std::endl;
				} else {
					// std::cout << m_name << " has parent (" << subtype_name << ")" << m_subtype << std::endl;
				}
			}



			// if (m_visibility == "UNDEFINED") {
			// 	std::regex_search(type_line, match, derive_regex);
			// 	m_derived_from
			// } else {
			// 	derive_regex = "\\[\\s*.+\\s+\\d+\\s*\\] "
			// 	std::regex_search(type_line, match, derive_regex);
			// }
			// if (!match[1].str().empty()) {
			// 	m_derived_from = match[1];
			// }
		}
	}

	std::string name() const {
		return m_name;
	}

	bool is_seq() const {
		return static_cast<bool>(m_sequence.size());
	}

	void debug_print() const {
		std::cout << "Type:\n\tName: " << m_name << std::endl;
		if (m_subtype != nullptr) {
			std::cout << "\tSubtype: " << m_subtype->m_name << std::endl;
			if (m_subtype->m_min_value != -1) {
				std::cout << "\tMin: " << m_subtype->m_min_value << std::endl;
				std::cout << "\tMax: " << m_subtype->m_max_value << std::endl;
			}
			if (m_subtype->m_size != -1) {
				std::cout << "\tSize: " << m_subtype->m_size << std::endl;
			}
		}
		if (m_min_value != -1) {
			std::cout << "\tMin value: " << m_min_value << "\n\tMax value: "<< m_max_value << std::endl;
		}
		if (m_size != -1) {
			std::cout << "\tSize: " << m_size << std::endl;
		}
		if (m_sequence.size()) {
			std::cout << "\tSequence:\n";
			for (auto& type : m_sequence) {
				std::cout << "\t\t" << type.m_name << std::endl;
				std::cout << "\t\t\t" << type.m_subtype_name << std::endl;
				if (type.m_min_value != -1) {
					std::cout << "\t\t\tMin: " << type.m_min_value  << std::endl;
					std::cout << "\t\t\tMax: " << type.m_max_value  << std::endl;
				}
			}
		}
		if (!m_visibility.empty()) {
			std::cout << "\tVisibility: " << m_visibility << " " << m_visibility_id << std::endl;
		}

	}
};

#endif